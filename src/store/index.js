import Vue from "vue";
import Vuex from "vuex";
import actions from "./actions";
import getters from "./getters";
import mutations from "./mutations";
import state from "./state";
import moduleCart from "./cart";
import moduleProduct from "./product";

Vue.use(Vuex);

const store = new Vuex.Store({
  namespaced: true,
  actions,
  mutations,
  getters,
  state,
  modules: {
    cart: moduleCart,
    product: moduleProduct
  }
});

export default store;
